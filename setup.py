import setuptools

with open("README.md", "r") as f:
    long_description = f.read()

setuptools.setup(
    name="clone-detector",
    author="Grammatech",
    description='A Python module to highlight duplicated code (i.e., "clones") in your editor.',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/GrammaTech/Mnemosyne/learn-synth/clone-detection",
    py_modules = ['server'],
    python_requires=">=3.6",
    classifiers=[
        "Intended Audience :: Developers",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
    ],
    entry_points={
        "console_scripts": [
            "clone-detector-server = server:main",
        ],
    },
    install_requires=[
        "pygments",
        "cchardet",
        "pygls @ git+https://gitlab.com/GrammaTech/Mnemosyne/learn-synth/pygls@master",
        "kitchensink @ git+https://gitlab.com/GrammaTech/Mnemosyne/learn-synth/kitchensink@master",
    ],
    extras_require={"dev": ["black", "pre-commit"]},
)

