from typing import Tuple, List, Dict, Optional

import asyncio
import collections
import tempfile
import os
import subprocess
import sys
import shutil
import time
from urllib.parse import urlparse

from pygls.server import LanguageServer
from pygls.lsp.methods import (
    INITIALIZE,
    TEXT_DOCUMENT_DID_OPEN,
    TEXT_DOCUMENT_DID_CLOSE,
    TEXT_DOCUMENT_DID_SAVE,
)
from pygls.lsp.types import (
    ArgotContentParams,
    ArgotFilesParams,
    DidOpenTextDocumentParams,
    DidCloseTextDocumentParams,
    DidSaveTextDocumentParams,
    Diagnostic,
    DiagnosticRelatedInformation,
    DiagnosticSeverity,
    InitializeParams,
    Location,
    Position,
    Range,
    TextDocumentIdentifier,
)

from kitchensink.utility.pygls import NonTerminatingLanguageServerProtocol
from kitchensink.utility.filesystem import guess_language
from kitchensink.utility.server import lsp_arg_parser

# file uri, start line, end line
ChunkInfo = Tuple[str, int, int]

# start and end in current file to a chunk in some other (or possibly same) file
CloneInfo = Dict[Tuple[int, int], List[ChunkInfo]]

DEFAULT_PORT = 3042

# Set to True iff the client (e.g. vscode) can support getting the file names and
# contents of the workspace.
argot_support = False


def merge(cur_clones: CloneInfo, to_add: CloneInfo) -> CloneInfo:
    for k, v in to_add.items():
        cur_clones[k].extend(v)


def uri_to_ext(uri: str) -> str:
    return os.path.splitext(urlparse(uri).path)[1]


def process_clones(clone_dir: str, cur_fn: str, fn_to_uri: Dict[int, str]) -> CloneInfo:
    """
    If there is only one file (which must be `cur_fn`), it will return all clones found
    within that file. Otherwise, it will return all clones found between `cur_fn` and
    some other file, but not clones entirely within `cur_fn` nor clones entirely between
    two other files.
    """
    uris = dict()
    cur_id = None

    with open(os.path.join(clone_dir, "files")) as filesfn:
        lncount = 0
        for line in filesfn:
            lncount += 1
            line = line.strip()
            if line.startswith("#"):
                continue

            lnparts = line.split()
            if len(lnparts) != 2:
                raise Exception(
                    "Line #", lncount, " has wrong number of parts: ", len(lnparts)
                )
            fnnum, fn = lnparts

            if fnnum in uris:
                raise Exception("Line #", lncount, " has duplicate number: ", fnnum)

            if fn == cur_fn:
                cur_id = fnnum

            if not fn in fn_to_uri:
                raise Exception(
                    "Line #", lncount, " has file ", fn, " with no corresponding uri"
                )

            uris[fnnum] = fn_to_uri[fn]

    if cur_id is None:
        raise Exception("Could not find file ", cur_fn, " in cwbuild 'files'")

    should_detect_same = len(uris) == 1

    with open(os.path.join(clone_dir, "clones")) as clonesfn:
        lncount = 0
        ret = collections.defaultdict(lambda: [])
        for line in clonesfn:
            lncount += 1
            line = line.strip()
            if line.startswith("#"):
                continue

            lnparts = line.split(",")
            if len(lnparts) != 6:
                raise Exception(
                    "Line #", lncount, " has wrong number of parts: ", len(lnparts)
                )
            fid1, s1, e1, fid2, s2, e2 = lnparts

            if not fid1 in uris or not fid2 in uris:
                raise Exception(
                    "Line #", lncount, " references invalid fn id ", fid1, " or ", fid2
                )
            fn1uri = uris[fid1]
            fn2uri = uris[fid2]

            if fid1 == cur_id:
                if fid2 != cur_id or should_detect_same:
                    ret[(s1, e1)].append((fn2uri, s2, e2))
            elif fid2 == cur_id:
                ret[(s2, e2)].append((fn1uri, s1, e1))

        return ret


def _run_in_dir(cmd, dir_) -> bool:
    rslt = subprocess.run(cmd, shell=True, cwd=dir_)
    if rslt.returncode == 0:
        return True
    else:
        print(
            "%s failed with return code %d:" % (cmd.split()[0], rslt.returncode),
            file=sys.stderr,
        )
        # TODO We ought to set capture_output=True and text=True, then we can use the code below
        #      to give a better error message, and remove the redirects to /dev/null from the commands.
        #      However, capture_output isn't introduced until python 3.7, so I guess it's not available
        #      for us ... ?
        #      print(rslt.stderr, file=sys.stderr)
        return False


def write_file(fn, source) -> None:
    with open(fn, "w") as f:
        f.write(source)


async def detect_clones(server: LanguageServer, cur_uri: str) -> Optional[CloneInfo]:
    global argot_support
    files_future = None

    def cancel_files_future():
        if files_future is not None:
            files_future.cancel()

    if argot_support:
        # kick off an async task to determine all other files in the workspace
        files_params = ArgotFilesParams()
        files_future = server.get_workspace_files_async(files_params)

    # TODO ugly hack, but there is some bug in cwbuild that makes it unable to be run from
    #      any directory but the install directory, even if INSTALL_DIR is set
    cwexe = shutil.which("cwbuild")
    if cwexe is None:
        print("Could not find cwbuild in path", file=sys.stderr)
        cancel_files_future()
        return None
    cw_dir = os.path.dirname(cwexe)

    # target dir is a subdirectory of a temp folder where source files will be kept
    # and which CloneWorks will be run on
    def target_dir(tmpdir: str) -> str:
        return os.path.join(tmpdir, "target")

    def mk_targetdir(tmpdir: str) -> str:
        targetdir = target_dir(tmpdir)
        os.mkdir(targetdir)
        return targetdir

    def runcw(tmpdir: str) -> bool:
        targetdir = target_dir(tmpdir)
        # "prepare the source code for clone detection", suppress output
        # set -c to be type3pattern (or perhaps type3line) for "aggressive", type3token for "conservative"
        rslt = _run_in_dir(
            "./cwbuild -i %s -f %s/files -b %s/fragments -l %s -g function -c type3token >/dev/null 2>/dev/null"
            % (targetdir, tmpdir, tmpdir, cur_lang),
            cw_dir,
        )
        if not rslt:
            return False

        # Do the actual detection
        return _run_in_dir(
            "./cwdetect -i %s/fragments -o %s/clones -s 0.7 -mil 3 >/dev/null 2>/dev/null"
            % (tmpdir, tmpdir),
            cw_dir,
        )

    with tempfile.TemporaryDirectory(prefix="lsp-clones-self-") as selftmpdir:
        # First, we'll copy the current file into a target directory, determine its
        # language, then run CloneWorks on it to find same-file clones.
        selftargetdir = mk_targetdir(selftmpdir)
        cur_basename = "cur" + uri_to_ext(cur_uri)
        cur_fn = os.path.join(selftargetdir, cur_basename)
        # TODO using the URI to grab file contents is expressly forbidden when the
        #      file has been 'open'd by some IDE. For now this is the most expedient
        #      solution, but ideally we would not use the uri but rather get the source
        #      from the didOpen or didSave params.
        cur_source = server.workspace.get_document(cur_uri).source
        write_file(cur_fn, cur_source)

        # this is used later to determine which other files are of the same language
        cur_lang = guess_language(cur_fn)
        if cur_lang is None:
            print(
                "Could not guess language of current file " + cur_uri,
                file=sys.stderr,
            )
            cancel_files_future()
            return None

        if not runcw(selftmpdir):
            print(
                "cwbuild or cwdetect returned non-zero error code for current file "
                + cur_uri,
                file=sys.stderr,
            )
            cancel_files_future()
            return None
        all_clones = process_clones(selftmpdir, cur_fn, {cur_fn: cur_uri})

        # If the client doesn't support argot, it can't get the data from the rest of
        # workspace, so we only return same-file clones.
        if files_future is None:
            return all_clones

        # Now, run through all other files, create a new directory for each one, and
        # copy it and the current file into that directory. If they're the same language,
        # run CloneWorks on them to detect clones between them.

        async def detect_in_other(other_uri: str) -> CloneInfo:
            with tempfile.TemporaryDirectory(prefix="lsp-clones-") as othertmpdir:
                othertargetdir = mk_targetdir(othertmpdir)
                cur_copy_fn = shutil.copy(cur_fn, othertargetdir)
                other_basename = "other" + uri_to_ext(other_uri)
                other_fn = os.path.join(othertargetdir, other_basename)
                content_params = ArgotContentParams(
                    text_document=TextDocumentIdentifier(uri=other_uri)
                )
                other = await server.get_text_document_content_async(content_params)
                write_file(other_fn, other["text"])

                if guess_language(other_fn) != cur_lang:
                    return {}

                if runcw(othertmpdir):
                    return process_clones(
                        othertmpdir,
                        cur_copy_fn,
                        {cur_copy_fn: cur_uri, other_fn: other_uri},
                    )
                else:
                    print(
                        "cwbuild or cwdetect returned non-zero error code when detecting clones in "
                        + other_uri,
                        file=sys.stderr,
                    )
                    return {}

        files = await files_future
        coroutines = [detect_in_other(f["uri"]) for f in files if f["uri"] != cur_uri]
        for o in await asyncio.gather(*coroutines):
            merge(all_clones, o)
        return all_clones


def mkRange(sline: int, eline: int) -> Range:
    sPos = Position(line=sline, character=0)
    ePos = Position(
        line=eline, character=1000 * 1000
    )  # rubberbands back to the last character on the line
    return Range(start=sPos, end=ePos)


def mkDiag(hereStart: int, hereEnd: int, others: List[ChunkInfo]) -> Diagnostic:
    hereRange = mkRange(hereStart, hereEnd)
    message = "This chunk of code looks suspiciously familiar ..."
    severity = DiagnosticSeverity.Information
    source = "CloneWorks"

    related_information = []
    for (otherUri, otherStart, otherEnd) in others:
        relinfo = DiagnosticRelatedInformation(
            location=Location(
                uri=otherUri,
                range=mkRange(otherStart, otherEnd),
            ),
            message="Another chunk of code that looks similar to this one",
        )
        related_information.append(relinfo)

    return Diagnostic(
        range=hereRange,
        message=message,
        severity=severity,
        source=source,
        related_information=related_information,
    )


def main():
    args_parser = lsp_arg_parser(__doc__, DEFAULT_PORT)
    args = args_parser.parse_args()

    if args.stdio:
        server = LanguageServer()
    else:
        server = LanguageServer(protocol_cls=NonTerminatingLanguageServerProtocol)

    async def publish_clone_diag(server: LanguageServer, uri: str) -> None:
        cloneinfo = await detect_clones(server, uri)
        if cloneinfo is not None:
            diags = [mkDiag(s, e, clones) for ((s, e), clones) in cloneinfo.items()]
            server.publish_diagnostics(uri, diags)

    @server.feature(INITIALIZE)
    def initialize(params: InitializeParams) -> None:
        global argot_support
        argot_support = (
            params.client_info and params.client_info.name == "Visual Studio Code"
        )

    @server.feature(TEXT_DOCUMENT_DID_OPEN)
    async def did_open(
        server: LanguageServer, params: DidOpenTextDocumentParams
    ) -> None:
        await publish_clone_diag(server, params.text_document.uri)

    @server.feature(TEXT_DOCUMENT_DID_CLOSE)
    def did_close(server: LanguageServer, params: DidCloseTextDocumentParams) -> None:
        # delete diagnostics by pushing the empty list
        server.publish_diagnostics(params.text_document.uri, [])

    @server.feature(TEXT_DOCUMENT_DID_SAVE)
    async def did_save(
        server: LanguageServer, params: DidSaveTextDocumentParams
    ) -> None:
        await publish_clone_diag(server, params.text_document.uri)

    if args.stdio:
        server.start_io()
    else:
        server.start_tcp("0.0.0.0", args.port)


if __name__ == "__main__":
    main()
