In order to run this server, [CloneWorks](https://github.com/jeffsvajlenko/CloneWorks) must be set up, so that its two executables `cwbuild` and `cwdetect` are on your path.
You can set up CloneWorks by following [these instructions](https://github.com/jeffsvajlenko/CloneWorks#Setup) and then patch in [this commit](https://gitlab.com/GrammaTech/Mnemosyne/learn-synth/CloneWorks/-/commit/0c20a6faae4260feffc9e8b9c79d8e770ac0f234) - however, those instructions have some bugs, so the revised instructions below are likely easier to follow:
1. **General Pre-reqs:** CloneWorks requires gcc, make, and jdk (7+). If any of these are not on your system, you'll have to install them.
1. **Python Pre-reqs:** You'll also need to `pip install pygments cchardet` if you haven't already.
1. **Install TXL:** CloneWorks requires FreeTXL 10.6 or later - if it's not already installed on your system, download it from: http://www.txl.ca/. The installer will give you the option to install it either in the system or in the user home directory - if you go with the latter approach, remember to add its location to your `$PATH`.
1. **Clone Repo:** `git clone` [this CloneWorks fork](https://gitlab.com/GrammaTech/Mnemosyne/learn-synth/CloneWorks) (there is not a binary that can be installed, so you have to use the source code). **NOTE** this is a small fork with only a few novel commits that should apply easily, so if you check [the original CloneWorks](https://github.com/jeffsvajlenko/CloneWorks) and find that it's been updated since Mar 24, 2021, you probably want to use the original and just patch the extra commits from [the fork](https://gitlab.com/GrammaTech/Mnemosyne/learn-synth/CloneWorks) onto it.
1. **Build:** Run `./build` from the CloneWorks directory in order to compile the Txl and java files. You will have to rebuild if you ever edit the Txl files.
1. **Test:** In the CloneWorks directory, run
`cwbuild -i example/JHotDraw54b1/ -f example/JHotDraw.files -b example/JHotDraw.fragments -l java -g function -c type3token`
and then
`./cwdetect -i example/JHotDraw.fragments -o example/JHotDraw.clones -s 0.7`  .
This should produce a ~58Kb file `example/JHotDraw.clones` where most lines are six comma-separated numbers (each such line represents a clone that was detected).
1. **Set PATH:** In the current shell and in `~/.bashrc`, set your `$PATH` environment variable to include the CloneWorks directory.
**NOTE!** due to a bug in CloneWorks, this doesn't actually allow you to execute `cwbuild` or `cwdetect` from arbitrary directories - they must only be executed from the CloneWorks directory. The clone detection server knows this and (in a system-independent way) uses the `$PATH` to determine what directory it must `cd` to before executing them.

Now that you have CloneWorks set up and working, it should be possible to run the server in the ordinary way.
